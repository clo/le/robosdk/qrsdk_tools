#Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear

#!/bin/bash

if [ ! -d /data/output ]
then
    mkdir /data/output
fi

echo "run demo.sh"

Main_Camera_Snapshot_4K60_Waylandsink_Display()
{
echo "run demo command is :export XDG_RUNTIME_DIR=/run/user/root && gst-pipeline-app -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 ! waylandsink fullscreen=true async=true sync=false camsrc.image_1 ! ""image/jpeg,width=3840,height=2160,framerate=30/1"" ! multifilesink location=/data/output/cam0_frame%d.jpg sync=true async=false"

export XDG_RUNTIME_DIR=/run/user/root && gst-pipeline-app -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 ! waylandsink fullscreen=true async=true sync=false camsrc.image_1 ! ""image/jpeg,width=3840,height=2160,framerate=30/1"" ! multifilesink location=/data/output/cam0_frame%d.jpg sync=true async=false
}


Main_Camera_Encode_4K60()
{
echo "run demo command is :export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0  -e qtiqmmfsrc name=camsrc ! video/x-raw\(memory:GBM\),format=NV12,width=3840,height=2160,framerate=60/1 ! queue ! omxh264enc control-rate=max-bitrate target-bitrate=6000000 interval-intraframes=29 periodicity-idr=1 ! queue ! h264parse ! mp4mux ! queue ! filesink location="/data/output/mux1.mp4""

export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0  -e qtiqmmfsrc name=camsrc ! video/x-raw\(memory:GBM\),format=NV12,width=3840,height=2160,framerate=60/1 ! queue ! omxh264enc control-rate=max-bitrate target-bitrate=6000000 interval-intraframes=29 periodicity-idr=1 ! queue ! h264parse ! mp4mux ! queue ! filesink location="/data/output/mux1.mp4"
}

Video_Decode_4K60_Waylandsink_Display()
{
echo "run demo conmmand is :export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 filesrc location=/data/output/mux1.mp4 ! qtdemux name=demux demux. ! queue ! h264parse ! omxh264dec  ! waylandsink fullscreen=true enable-last-sample=false"

export export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 filesrc location=/data/output/mux1.mp4 ! qtdemux name=demux demux. ! queue ! h264parse ! omxh264dec  ! waylandsink fullscreen=true enable-last-sample=false
}

Main_Camera_Preview_4K60_Waylandsink_Display()
{
echo "run demo commond line is : gst-launch-1.0 -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 !  waylandsink fullscreen=true async=true sync=false"

export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 -e qtiqmmfsrc camera=0 name=camsrc ! video/x-raw,format=NV12,width=3840,height=2160,framerate=60/1 !  waylandsink fullscreen=true async=true sync=false
}

Audio_PCM_Record()
{
echo "run demo command is "export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 -v pulsesrc ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! audioconvert ! wavenc ! filesink location=/data/track0.wav""

export XDG_RUNTIME_DIR=/run/user/root && gst-launch-1.0 -v pulsesrc ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! audioconvert ! wavenc ! filesink location=/data/track0.wav
}

Audio_PCM_Playback()
{
echo "run command is "gst-launch-1.0 filesrc location=/data/track0.wav ! wavparse ! audioconvert ! pulsesink""
gst-launch-1.0 filesrc location=/data/track0.wav ! wavparse ! audioconvert ! pulsesink
}

Weston_simple_egl()
{
echo "run command is "export XDG_RUNTIME_DIR=/run/user/root && weston-simple-egl""
export XDG_RUNTIME_DIR=/run/user/root && weston-simple-egl
}


if [[ $1 =~ "Main_Camera_Preview_4K60_Waylandsink_Display" ]];then
    Main_Camera_Preview_4K60_Waylandsink_Display
elif [[ $1 =~ "Main_Camera_Snapshot_4K60_Waylandsink_Display" ]];then
    Main_Camera_Snapshot_4K60_Waylandsink_Display
elif [[ $1 =~ "Main_Camera_Encode_4K60" ]];then
    Main_Camera_Encode_4K60
elif [[ $1 =~ "Video_Decode_4K60_Waylandsink_Display"  ]];then
    Video_Decode_4K60_Waylandsink_Display
elif [[ $1 =~ "Audio_PCM_Record"   ]];then
    Audio_PCM_Record
elif [[ $1 =~ "Audio_PCM_Playback"   ]];then
    Audio_PCM_Playback
elif [[ $1 =~ "Weston_simple_egl"   ]];then
    Weston_simple_egl
else
    echo "please run again and enter demo name : "
    echo "Weston_simple_egl"
    echo "Audio_PCM_Playback"
    echo "Audio_PCM_Record"
    echo "Video_Decode_4K60_Waylandsink_Display"
    echo "Main_Camera_Encode_4K60"
    echo "Main_Camera_Snapshot_4K60_Waylandsink_Display"
    echo "Main_Camera_Preview_4K60_Waylandsink_Display"
fi
